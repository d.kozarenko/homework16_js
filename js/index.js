"use strict";

let firstNumber = prompt("Enter first number: ");
let secondNumber = prompt("Enter second number: ");
while (!Number.isInteger(+firstNumber) || !Number.isInteger(+secondNumber) || firstNumber.trim() === "" || secondNumber.trim() === "") {
    console.log("Error. Try again");
    firstNumber = prompt("Enter first number: ", firstNumber);
    secondNumber = prompt("Enter second number: ", secondNumber);
}
let n = prompt("Enter index of number you want to find: ");
while (!Number.isInteger(+n) || n.trim() === "") {
    console.log("Error. Try again");
    n = prompt("Enter index of number you want to find: ", n);
}
console.log(fibonacciSearch(+firstNumber, +secondNumber, +n));
function fibonacciSearch(a, b, index) {
    let c;
    for (let counter = 0; counter <= index; counter++) {
        if (counter === index) {
            break;
        }
        c = a + b;
        a = b;
        b = c;
    }
    return c;
}